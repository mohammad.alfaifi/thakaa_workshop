# Thakaa Workshop 


This repo contains everything that was discussed in Thakaa's workship. `Rway.Rmd` file has everything you need. 

## locally 

In order to follow everything in this workshop and work on this locally:

- Install R
- Install Rstudio 

Here is a good guide:https://rstudio-education.github.io/hopr/starting.html 

## Cloud

If you want to try this out on the cloud, you can just sign up on rstudio cloud from here: https://rstudio.cloud/ 

## Requirments 

Whether you are working on this on the cloud or locally,  you need install the following packages in order for you to follow through:

- `install.packages("tidyverse")`;
- `install.packages("tidymodels")`;
- `install.packages("janitor")` ;
- `install.packages("doParallel")`;
- `install.packages("patchwork")`;
